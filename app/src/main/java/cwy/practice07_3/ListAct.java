package cwy.practice07_3;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ListAct extends AppCompatActivity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        loadDB();
    }
    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }

    public void loadDB() {
        SQLiteDatabase db = openOrCreateDatabase (
                "hw.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS people " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, no INTEGER, name TEXT, age INTEGER);");
        Cursor c = db.rawQuery("SELECT * FROM people;", null);
        startManagingCursor (c);
        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c,
                new String[] {"no", "name","age"},
                new int[] {R.id.text1, R.id.text2, R.id.text3}, 0
        );

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapt);

        if (db != null) {
            db.close();
        }
    }
}
