package cwy.practice07_3;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsAct extends AppCompatActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins);

        Button btn = (Button) findViewById(R.id.button1);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText);
        String no = txt.getText().toString();
        txt = (EditText)findViewById(R.id.editText1);
        String name = txt.getText().toString();
        txt = (EditText)findViewById(R.id.editText2);
        String age = txt.getText().toString();
        SQLiteDatabase db = openOrCreateDatabase (
                "hw.db",
                SQLiteDatabase.CREATE_IF_NECESSARY ,
                null
        );

        Cursor c = db.rawQuery("SELECT * FROM people where no=" + no + ";", null);
        if (c.getCount() != 0) {
            Toast.makeText(this, "중복된 학번입니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        else if (no.length() != 10 || no.startsWith("0")) {
            Toast.makeText(this, "학번은 10자리 이하, 앞 4자리가 실제년도 입니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        boolean err;
        for (int i = 0; i < age.length(); i++) {
            if (age.charAt(i) > '0' || age.charAt(i) < '9') {
                err = true;
                break;
            }
        }
        if (name.length() > 20) {
            Toast.makeText(this, "이름은 20자 이내 한글, 영어로 이루어집니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        err = false;
        for (int i = 0; i < age.length(); i++) {
            if (age.charAt(i) < '0' || age.charAt(i) > '9') {
                err = true;
                break;
            }
        }
        if (err || Integer.parseInt(age) > 200) {
            Toast.makeText(this, "나이는 200이내 입니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        String sql = "INSERT INTO people (no,name,age) VALUES (" + no + ",'" + name + "'," + age + ");";
        db.execSQL(sql);
        finish();
    }
}
