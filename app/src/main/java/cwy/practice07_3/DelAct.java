package cwy.practice07_3;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

public class DelAct extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del);

        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(this);

        SQLiteDatabase db = openOrCreateDatabase (
                "hw.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS people " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, no INTEGER, name TEXT, age INTEGER);");
        Cursor c = db.rawQuery("SELECT * FROM people;", null);
        startManagingCursor (c);
        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c,
                new String[] {"no", "name","age"},
                new int[] {R.id.text1, R.id.text2, R.id.text3}, 0
        );

        ListView listView = (ListView) findViewById(R.id.listDel);
        listView.setAdapter(adapt);

        if (db != null) {
            db.close();
        }
    }

    @Override
    public void onClick(View v) {
        EditText txt = null;
        txt = (EditText)findViewById(R.id.editText3);
        String no = txt.getText().toString();

        String sql = "DELETE FROM people WHERE no=" + no + ";";
        SQLiteDatabase db = openOrCreateDatabase (
                "hw.db",
                SQLiteDatabase.CREATE_IF_NECESSARY ,
                null
        );
        db.execSQL(sql);

        Cursor c = db.rawQuery("SELECT * FROM people;", null);
        startManagingCursor (c);
        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c,
                new String[] {"no", "name","age"},
                new int[] {R.id.text1, R.id.text2, R.id.text3}, 0
        );

        ListView listView = (ListView) findViewById(R.id.listDel);
        listView.setAdapter(adapt);

        if (db != null) {
            db.close();
        }
    }
}
